package csd203.michael.walsh.texttospeech;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SmsBroadcastReceiver extends BroadcastReceiver {

    public static final String SMS_BUNDLE = "pdus";


    public void onReceive(Context context, Intent intent)
    {
        Bundle intentExtras = intent.getExtras();

        System.out.println("Got something");

        if(intentExtras != null)
        {
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);

            for(int i = 0; i < sms.length; ++i)
            {
                String format = intentExtras.getString("format");
                SmsMessage smsMessage = SmsMessage.createFromPdu( (byte[]) sms[i], format );

                String smsBody = smsMessage.getMessageBody().toString();
                String address = smsMessage.getOriginatingAddress();

                MainActivity.instance.ReceivedMessage(smsBody, address);
            }


        }
    }
}
