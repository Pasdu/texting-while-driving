package csd203.michael.walsh.texttospeech;

import android.Manifest;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static MainActivity instance;
    public TTSInitter tts;
    private String responseMessage;
    private int MY_PERMISSIONS_REQUEST_SMS_RECEIVE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.SEND_SMS},
                MY_PERMISSIONS_REQUEST_SMS_RECEIVE);



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tts = new TTSInitter(this);
        instance = this;
        Button setMsgBtn = ((Button)findViewById(R.id.changeMessage));
        setMsgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetResponse( ((EditText) findViewById(R.id.messageText)).getText().toString() );
            }
        });


    }


    public void ReceivedMessage(final String smsMessageStr, final String smsAddress) {
        ((TextView) findViewById(R.id.messageDisplay)).setText(smsMessageStr);

        if(((CheckBox) findViewById(R.id.ttsEnabled)).isChecked())
            tts.speak(smsMessageStr, true);
        if(((CheckBox) findViewById(R.id.autoResponse)).isChecked())
            SendResponse(smsMessageStr, smsAddress);
    }

    public void SetResponse(String msg)
    {
        responseMessage = msg;
        ((TextView) findViewById(R.id.autoResponseDisplay)).setText(msg);
    }

    public void SendResponse(String dest,  String message)
    {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(dest, null, message, null, null);
    }

}
