package csd203.michael.walsh.texttospeech;

import android.content.Context;
import android.speech.tts.TextToSpeech;

public class TTSInitter implements TextToSpeech.OnInitListener {

    private TextToSpeech tts;
    private boolean ttsOk;

    TTSInitter(Context context)
    {
        tts = new TextToSpeech(context, this);
    }


    @Override
    public void onInit(int status)
    {
        if (status == TextToSpeech.SUCCESS)
        {
            ttsOk = true;
        }
        else
        {
            ttsOk = false;
        }
    }

    @SuppressWarnings("deprecation")
    public void speak(String text, Boolean override)
    {
        if(ttsOk)
        {
            if(override)
            {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
            }
            else
            {
                tts.speak(text, TextToSpeech.QUEUE_ADD, null);
            }
        }
    }

}
